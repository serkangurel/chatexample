package com.serkangurel.chatexample.repository

data class Message(
    val message: String? = null,
    val name: String? = null,
    val timestamp: Long? = null,
    val type: String? = null,
    val userId: String? = null
)