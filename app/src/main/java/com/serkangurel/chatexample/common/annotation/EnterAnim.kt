package com.serkangurel.chatexample.common.annotation

import androidx.annotation.AnimRes

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
annotation class EnterAnim(

    @AnimRes
    val enterAnim: Int,

    @AnimRes
    val exitAnim: Int

)