package com.serkangurel.chatexample.base

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.serkangurel.chatexample.common.annotation.EnterAnim
import com.serkangurel.chatexample.common.annotation.ExitAnim
import com.serkangurel.chatexample.common.annotation.SetLayout
import com.serkangurel.chatexample.common.extensions.getAnnotation

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var binding: ViewDataBinding

    private var setLayout: SetLayout? = null
    private var screenName: String? = null

    private var enterAnim: EnterAnim? = null
    private var exitAnim: ExitAnim? = null

    init {
        setLayout = getAnnotation(true) {
            screenName = it.screenName
            if (it.value == 0)
                throw RuntimeException("@SetLayout has invalid value " + javaClass.name)
        }
        enterAnim = getAnnotation()
        exitAnim = getAnnotation()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        enterAnim?.let { overridePendingTransition(it.enterAnim, it.exitAnim) }
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, setLayout!!.value)
        binding.lifecycleOwner = this
        //findViewById<Toolbar>(R.id.toolbar)?.apply { setSupportActionBar(this) }
        sendScreenName(screenName)
    }

    @Suppress("UNCHECKED_CAST")
    protected fun <T : ViewDataBinding> getBinding(): T {
        return binding as T
    }

//    protected fun bindModel(value: Any?, variableId: Int = BR.viewModel) {
//        binding.setVariable(variableId, value)
//        binding.executePendingBindings()
//    }

    protected fun sendScreenName(screenName: String?) {
        if (!screenName.isNullOrEmpty()) {
            println(screenName)
        }
    }

    fun replaceFragment(fragment: Fragment, @IdRes containerId: Int) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(containerId, fragment, fragment.javaClass.simpleName)
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun finish() {
        super.finish()
        exitAnim?.let { overridePendingTransition(it.enterAnim, it.exitAnim) }
    }

}
