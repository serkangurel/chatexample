package com.serkangurel.chatexample.base

import android.app.Application
import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.AndroidViewModel
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    val disposable = CompositeDisposable()
    var resources: Resources

    init {
        resources = application.resources
    }

    override fun onCleared() {
        super.onCleared()
        if (disposable.size() > 0) {
            disposable.dispose()
        }
    }

    protected fun getString(@StringRes resId: Int): String {
        return getApplication<Application>().getString(resId)
    }

    protected fun getString(@StringRes resId: Int, vararg formatArgs: Any): String {
        return getApplication<Application>().getString(resId, *formatArgs)
    }
}