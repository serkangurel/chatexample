package com.serkangurel.chatexample.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.serkangurel.chatexample.repository.Message
import java.text.SimpleDateFormat
import java.util.*


class MessageAdapter(list: List<Message>? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val messageList = arrayListOf<Message>()

    init {
        list?.let { messageList.addAll(it) }
    }

    fun setList(list: List<Message>?) {
        if (list != null) {
            messageList.clear()
            messageList.addAll(list)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view =
            inflater.inflate(com.serkangurel.chatexample.R.layout.item_message, parent, false)
        return MessageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return messageList.size
    }

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val messageViewHolder = holder as MessageViewHolder
        val message = messageList.get(position)
        messageViewHolder.tvUserName.setText(message.name)
        messageViewHolder.tvMessageBody.setText(message.message)

        val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss", Locale("tr"))
        val calendar = Calendar.getInstance().also {
            it.timeInMillis = message.timestamp!!
        }
        messageViewHolder.tvMessageTime.setText(formatter.format(calendar.getTime()))
    }

    inner class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvUserName: TextView
        var tvMessageBody: TextView
        var tvMessageTime: TextView

        init {
            tvUserName = view.findViewById(com.serkangurel.chatexample.R.id.text_user_name)
            tvMessageBody = view.findViewById(com.serkangurel.chatexample.R.id.text_message_body)
            tvMessageTime = view.findViewById(com.serkangurel.chatexample.R.id.text_message_time)
        }
    }
}