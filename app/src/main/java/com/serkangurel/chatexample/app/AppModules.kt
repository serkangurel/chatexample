package com.serkangurel.chatexample.app

import com.serkangurel.chatexample.ui.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object AppModules {

    val appModules: Module = module {
        viewModel { MainViewModel(get()) }
    }
}