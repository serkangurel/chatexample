package com.serkangurel.chatexample.ui

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.serkangurel.chatexample.R
import com.serkangurel.chatexample.adapter.MessageAdapter
import com.serkangurel.chatexample.base.BaseActivity
import com.serkangurel.chatexample.common.annotation.SetLayout
import com.serkangurel.chatexample.common.extensions.hideSoftKeyboard
import com.serkangurel.chatexample.databinding.ActivityMainBinding
import com.serkangurel.chatexample.repository.Message
import org.koin.android.ext.android.inject

@SetLayout(R.layout.activity_main)
class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private val adaper = MessageAdapter()
    private val viewModel: MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getBinding()
        binding.viewModel = viewModel
        binding.reyclerview.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.reyclerview.adapter = adaper

        viewModel.messageList.observe(this, Observer {
            binding.groupLoading.visibility = View.GONE
            binding.reyclerview.smoothScrollToPosition(it.size - 1)
            adaper.setList(it)
        })

        binding.buttonChatboxSend.setOnClickListener {
            val messageBody = binding.edittextChatbox.text.toString()
            if (!messageBody.isBlank()) {
                val newMesssage = Message(
                    messageBody,
                    "mobile_client",
                    System.currentTimeMillis(),
                    "default",
                    "userId"
                )
                viewModel.sendNewMessage(newMesssage)

                binding.edittextChatbox.text.clear()
                hideSoftKeyboard(this)
            }
        }
    }
}