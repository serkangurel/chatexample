package com.serkangurel.chatexample.ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.serkangurel.chatexample.base.BaseViewModel
import com.serkangurel.chatexample.common.constant.Constants
import com.serkangurel.chatexample.repository.Message

class MainViewModel(application: Application) : BaseViewModel(application) {

    var messageList = MutableLiveData<List<Message>>()
    private val mMessageList = arrayListOf<Message>()
    private val database = FirebaseDatabase.getInstance()
    private val myRef = database.getReference("room-messages").child(Constants.ROOM_ID)

    init {
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                mMessageList.clear()
                dataSnapshot.children.forEach {
                    val message = it.getValue(Message::class.java)
                    message?.let { mMessageList.add(it) }
                }
                val sortedList =
                    mMessageList.sortedBy { chatMessage: Message -> chatMessage.timestamp }
                messageList.postValue(sortedList)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.e("error", "Failed to read value.", error.toException())
            }
        })
    }

    fun sendNewMessage(message: Message) {
        //val map: Map<String, Message> = hashMapOf("randomId" to message)
        myRef.push().setValue(message)
    }

}